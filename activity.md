# Initialize Weekly Menu View

```plantuml
start
if (push button) then (new)
    :make new weekly menu;
else (load)
    :get last one month weekly collection;
    if (collection num > 1) then (yes)
        :popup selector;
    elseif (collection num) then (1)
    else (0)
        end
    endif
endif
:show weekly menu;
stop
```
