import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../util/firestore.dart';

enum MealType {
  breakfast,
  lunch,
  dinner,
}

class WeeklyMenuList {
  Widget _list = Text('loadgin');

  List<String> growUp7DaysMenu(final List<String> menuList) {
    switch (menuList.length) {
      case 7:
        {
          return menuList;
        }
      case 6:
        {
          return [...menuList, menuList[0]];
        }
      case 5:
        {
          return [...menuList, menuList[0], menuList[1]];
        }
      case 4:
        {
          return [...menuList, menuList[0], menuList[1], menuList[2]];
        }
      case 3:
        {
          return [...menuList, ...menuList, menuList[0]];
        }
      case 2:
        {
          return [...menuList, ...menuList, ...menuList, menuList[0]];
        }
      case 1:
        {
          return [
            menuList[0],
            menuList[0],
            menuList[0],
            menuList[0],
            menuList[0],
            menuList[0],
            menuList[0]
          ];
        }
      default:
        {
          return List.filled(7, '-');
        }
    }
  }

  Widget makeMenuContainer(MealType mealType, String menuTitle) {
    Widget icon;
    switch (mealType) {
      case MealType.breakfast:
        {
          icon = Icon(
            Icons.brightness_low,
            color: Colors.orange,
          );
        }
        break;

      case MealType.lunch:
        {
          icon = Icon(
            Icons.lunch_dining,
            color: Colors.brown,
          );
        }
        break;

      case MealType.dinner:
        {
          icon = Icon(
            Icons.bedtime,
            color: Colors.yellow,
          );
        }
        break;

      default:
        {
          icon = Icon(Icons.bedtime);
        }
    }

    return Container(
      height: 30,
      child: Row(children: <Widget>[
        icon,
        Text(menuTitle),
      ]),
    );
  }

  Future<List<String>> queryMenu(CollectionReference collection,
      MealType mealType, FirestoreAccessor firestoreAccessor) async {
    var mealTypeToString = mealType
        .toString()
        .replaceFirst(mealType.toString().split('.')[0] + '.', '');

    var menuList = await firestoreAccessor.queryMenuAtRandom(mealTypeToString);

    return growUp7DaysMenu(menuList);
  }

  Future<void> make(CollectionReference collection,
      FirestoreAccessor firestoreAccessor) async {
    var breakfastMenu =
        await queryMenu(collection, MealType.breakfast, firestoreAccessor);
    var lunchMenu =
        await queryMenu(collection, MealType.lunch, firestoreAccessor);
    var dinnerMenu =
        await queryMenu(collection, MealType.dinner, firestoreAccessor);

    if (breakfastMenu == null || lunchMenu == null || dinnerMenu == null) {
      return;
    }

    var menuContainers = <Widget>[];
    var i = 0;
    ['Mon.', 'Tue.', 'Wed', 'Thu.', 'Fri.', 'Sat.', 'Sun.'].forEach((day) {
      menuContainers.add(Text(day));
      menuContainers
          .add(makeMenuContainer(MealType.breakfast, breakfastMenu[i]));
      menuContainers.add(makeMenuContainer(MealType.lunch, lunchMenu[i]));
      menuContainers.add(makeMenuContainer(MealType.dinner, dinnerMenu[i]));

      i++;
    });

    _list = ListView(
      padding: const EdgeInsets.all(8),
      children: menuContainers,
    );
  }

  List<String> makeThisWeek(int start, int length) {
    var made = <String>[];

    if (start > 7 || start < 0) {
      return made;
    }

    final repeatnum = (start + length) ~/ 7;
    for (var i = 0; i < (repeatnum + 1); i++) {
      made = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', ...made];
    }

    return made.getRange(start, start + length).toList();
  }

  Future<void> loadWeeklyMenu(CollectionReference collection, String docID,
      String weeklyMenuStartDate, FirestoreAccessor firestoreAccessor) async {
    final breakfastMenu = await firestoreAccessor.queryWeeklyMenu(docID);

    final thisweek = makeThisWeek(
        DateTime.parse(weeklyMenuStartDate).weekday - 1, breakfastMenu.length);

    var menuContainers = <Widget>[];
    var i = 0;
    thisweek.forEach((day) {
      menuContainers.add(Text(day));
      menuContainers
          .add(makeMenuContainer(MealType.breakfast, breakfastMenu[i]));
      menuContainers.add(makeMenuContainer(MealType.lunch, 'll'));
      menuContainers.add(makeMenuContainer(MealType.dinner, 'dd'));

      i++;
    });

    _list = ListView(
      padding: const EdgeInsets.all(8),
      children: menuContainers,
    );
  }
}

class WeeklyMenu extends StatefulWidget {
  final FirestoreAccessor firestoreAccessor;
  final double _appBarHeight;
  final double _bottomBarHeight;

  WeeklyMenu(this.firestoreAccessor, this._appBarHeight, this._bottomBarHeight);

  @override
  _WeeklyMenuState createState() => _WeeklyMenuState();
}

class _WeeklyMenuState extends State<WeeklyMenu> {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final String documentId = 'test';

  var state = 'load';
  String weeklyMenuDocID;
  String weeklyMenuStartDate;

  @override
  Widget build(BuildContext context) {
    final collection = FirebaseFirestore.instance
        .collection('users')
        .doc(documentId)
        .collection('menu');

    final weeklyMenuList = WeeklyMenuList();

    final topButtonsHeight = 40.0;
    final bodyHeight = MediaQuery.of(context).size.height -
        topButtonsHeight -
        widget._appBarHeight -
        widget._bottomBarHeight;

    final topButtons = Container(
        height: topButtonsHeight,
        child: Row(children: <Widget>[
          OutlinedButton(
            onPressed: () {
              state = 'new';
              setState(() {});
            },
            child: Text('New'),
          ),
          OutlinedButton(
            onPressed: () {
              widget.firestoreAccessor
                  .queryWeeklyMenuStartDates()
                  .then((starts) {
                var options = <Widget>[];
                starts.forEach((docIDofAWeeklyMenu, startDate) =>
                    options.add(SimpleDialogOption(
                      onPressed: () {
                        weeklyMenuStartDate = startDate;
                        Navigator.pop(context, docIDofAWeeklyMenu);
                      },
                      child: Text(startDate),
                    )));

                return showDialog<String>(
                  context: context,
                  barrierDismissible: true,
                  builder: (context) {
                    return SimpleDialog(
                      children: options,
                    );
                  },
                ).then((selectedDocID) {
                  setState(() {
                    weeklyMenuDocID = selectedDocID;
                    state = 'load';
                  });
                });
              });
            },
            child: Text('Load'),
          ),
        ]));

    Widget body;
    Widget buildWeeklyMenuView(
        BuildContext context, AsyncSnapshot<void> snapshot) {
      Widget child;
      if (snapshot.hasError) {
        child = Text('Something went wrong');
      } else if (snapshot.connectionState == ConnectionState.done) {
        child = weeklyMenuList._list;
      } else {
        child = Text('Loading');
      }

      return Container(height: bodyHeight, child: child);
    }

    if (state == 'initial') {
      body = Container(height: bodyHeight);
    } else if (state == 'new') {
      body = FutureBuilder(
        future: weeklyMenuList.make(collection, widget.firestoreAccessor),
        builder: buildWeeklyMenuView,
      );
    } else if (state == 'load') {
      body = FutureBuilder(
        future: weeklyMenuList.loadWeeklyMenu(collection, weeklyMenuDocID,
            weeklyMenuStartDate, widget.firestoreAccessor),
        builder: buildWeeklyMenuView,
      );
    }

    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minHeight: topButtonsHeight,
        ),
        child: IntrinsicHeight(
          child: Column(
            children: <Widget>[
              Container(height: topButtonsHeight, child: topButtons),
              Expanded(child: body),
            ],
          ),
        ),
      ),
    );
  }
}
