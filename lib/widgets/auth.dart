import 'package:flutter/material.dart';

import '../util/firestore.dart';

Widget signInDialog(BuildContext context, FirestoreAccessor firestoreAccessor) {
  final _key = GlobalKey<FormState>();
  String _email;
  String _password;

  return AlertDialog(
    title: Text('Sign-in'),
    content: Form(
      key: _key,
      child: Column(
        children: <Widget>[
          Text(
            'Email',
          ),
          TextFormField(
            onSaved: (value) {
              _email = value;
            },
          ),
          Text(
            'Password',
          ),
          TextFormField(
            onSaved: (value) {
              _password = value;
            },
          ),
          OutlinedButton(
            onPressed: () async {
              _key.currentState.save();

              if (await firestoreAccessor.trySignIn(_email, _password)) {
                Navigator.of(context).pop();
              }
            },
            child: Text('Sign in'),
          ),
          OutlinedButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Cancel'),
          ),
        ],
      ),
    ),
  );
}

Widget signOutDialog(
    BuildContext context, FirestoreAccessor firestoreAccessor) {
  return AlertDialog(
    title: Text('You Sined In'),
    content: SingleChildScrollView(
      child: ListBody(
        children: <Widget>[
          Text('Sign Out function is not implemented.'),
        ],
      ),
    ),
    actions: <Widget>[
      TextButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text('OK'),
      ),
    ],
  );
}
