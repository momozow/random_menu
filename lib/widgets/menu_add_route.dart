import 'package:flutter/material.dart';

import '../util/firestore.dart';
import './menu_tags.dart';

class AddMenuRoute extends StatelessWidget {
  final FirestoreAccessor firestoreAccessor;
  AddMenuRoute(this.firestoreAccessor);

  @override
  Widget build(BuildContext context) {
    final _key = GlobalKey<FormState>();
    final _menuTypes =
        MenuTagsFilterChip(tags: ['Breakfast', 'Lunch', 'Dinner']);
    final _menuCharacter = MenuTagsFilterChip(tags: ['Hot Cook', 'Quick']);
    String _reference;
    String _title;

    return Scaffold(
      appBar: AppBar(title: Text('Add Menu')),
      body: Form(
        key: _key,
        child: Column(
          children: <Widget>[
            Text(
              'Menu Title',
            ),
            TextFormField(
              onSaved: (value) {
                _title = value;
              },
            ),
            Text(
              'Reference',
            ),
            TextFormField(
              onSaved: (value) {
                _reference = value;
              },
            ),
            Text(
              'Menu Type',
            ),
            _menuTypes,
            Text(
              'Menu Tag',
            ),
            _menuCharacter,
            OutlinedButton(
              onPressed: () async {
                _key.currentState.save();

                var snapshot = await firestoreAccessor.querySnapshot();
                await firestoreAccessor.addMenu(
                    snapshot.size,
                    _title,
                    _reference,
                    _menuTypes.selectedTags,
                    _menuCharacter.selectedTags);
              },
              child: Text('Add'),
            ),
          ],
        ),
      ),
    );
  }
}
