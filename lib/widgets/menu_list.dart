import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../util/firestore.dart';
import './menu_add_route.dart';

Widget attachHeader(BuildContext context, Widget beAttached,
    FirestoreAccessor firestoreAccessor) {
  return Column(
    children: <Widget>[
      Row(
        children: <Widget>[
          Text('Menu List '),
          IconButton(
              icon: Icon(Icons.add_circle),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute<void>(builder: (context) {
                    return AddMenuRoute(firestoreAccessor);
                  }),
                );
              }),
        ],
      ),
      Flexible(
        child: beAttached,
      ),
    ],
  );
}

class MenuList extends StatelessWidget {
  final FirestoreAccessor firestoreAccessor;
  MenuList(this.firestoreAccessor);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
      future: firestoreAccessor.querySnapshot(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.done) {
          final List<Widget> menuContainers = snapshot.data.docs.map((doc) {
            final menuTitle = doc.get('title').toString();
            return Container(
              height: 30,
              child: Text(menuTitle),
            );
          }).toList();

          final Widget menuTitles = ListView(
            padding: const EdgeInsets.all(8),
            children: menuContainers,
          );

          return attachHeader(context, menuTitles, firestoreAccessor);
        }

        return attachHeader(context, Text('loading'), firestoreAccessor);
      },
    );
  }
}
