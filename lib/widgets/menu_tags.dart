import 'package:flutter/material.dart';

class MenuTagsFilterChip extends StatefulWidget {
  final List<String> tags;
  final List<String> selectedTags = <String>[];

  MenuTagsFilterChip({
    @required this.tags,
  });

  @override
  State createState() => MenuTagsFilterChipState();
}

class MenuTagsFilterChipState extends State<MenuTagsFilterChip> {
  Iterable<Widget> get tagWidgets sync* {
    for (var tag in widget.tags) {
      yield Padding(
        padding: const EdgeInsets.all(4.0),
        child: FilterChip(
          avatar: CircleAvatar(child: Icon(Icons.circle)),
          label: Text(tag),
          selected: widget.selectedTags.contains(tag),
          onSelected: (value) {
            setState(() {
              if (value) {
                widget.selectedTags.add(tag);
              } else {
                widget.selectedTags.removeWhere((name) {
                  return name == tag;
                });
              }
            });
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Wrap(
          children: tagWidgets.toList(),
        ),
      ],
    );
  }
}
