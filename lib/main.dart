import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

import './util/firestore.dart';
import './widgets/auth.dart';
import './widgets/menu_list.dart';
import './widgets/weekly_menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
      title: 'Menu List',
    );
  }
}

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  bool _initialized = false;
  bool _error = false;
  int _selectedIndex = 0;

  FirestoreAccessor firestoreAccessor = FirestoreAccessor();

  Future<void> initializeFlutterFire() async {
    try {
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
    } catch (e) {
      setState(() {
        _error = true;
      });
    }
  }

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  static const List<BottomNavigationBarItem> _bottomNavigationBarItems = [
    BottomNavigationBarItem(
        icon: Icon(Icons.restaurant_menu), label: 'Menu List'),
    BottomNavigationBarItem(
        icon: Icon(Icons.calendar_today), label: 'Weekly Menu'),
  ];

  void _onBottomNavigationItemTapped(final int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget _userIcon() {
    return Padding(
      padding: EdgeInsets.only(right: 20.0),
      child: GestureDetector(
        onTap: () {
          showDialog<void>(
            context: context,
            barrierDismissible: true,
            builder: (context) {
              if (firestoreAccessor.userCredential == null) {
                return signInDialog(context, firestoreAccessor);
              } else {
                print(firestoreAccessor.userCredential.user.email);
                return signOutDialog(context, firestoreAccessor);
              }
            },
          );
        },
        child: Icon(
          Icons.account_circle,
          color: Colors.grey,
          size: 26.0,
        ),
      ),
    );
  }

  Widget _body(
      int _selectedIndex, double _appBarHeight, double _bottomBarHeight) {
    switch (_selectedIndex) {
      case 0:
        {
          return MenuList(firestoreAccessor);
        }

      case 1:
        {
          return WeeklyMenu(firestoreAccessor, _appBarHeight, _bottomBarHeight);
        }

      default:
        {
          return Text('test');
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_error) {
      //return SomethingWentWrong();
    }

    if (!_initialized) {
      //return Loading();
    }

    final _appBar = AppBar(
      title: const Text('Random Menu'),
      actions: <Widget>[
        _userIcon(),
      ],
    );

    final _bottomBar = BottomNavigationBar(
      currentIndex: _selectedIndex,
      items: _bottomNavigationBarItems,
      onTap: _onBottomNavigationItemTapped,
      selectedItemColor: Colors.amber[800],
    );

    return Scaffold(
      appBar: _appBar,
      body: Center(
        child: _body(_selectedIndex, _appBar.preferredSize.height,
            kBottomNavigationBarHeight),
      ),
      bottomNavigationBar: _bottomBar,
    );
  }
}
