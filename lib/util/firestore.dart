import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirestoreAccessor {
  final documentId = 'test';
  final users = FirebaseFirestore.instance.collection('users');

  UserCredential userCredential;

  FirebaseAuth auth = FirebaseAuth.instance;

  QuerySnapshot menuSnapshot;
  QuerySnapshot weeklyMenuSnapshot;

  Future<QuerySnapshot> querySnapshot() async {
    menuSnapshot ??= await users.doc(documentId).collection('menu').get();

    return menuSnapshot;
  }

  Future<QuerySnapshot> queryWeeklyMenuSnapshot() async {
    weeklyMenuSnapshot ??=
        await users.doc(documentId).collection('weekly').get();

    return weeklyMenuSnapshot;
  }

  Future<List<String>> queryMenuAtRandom(String mealType) {
    return querySnapshot().then((snapshot) {
      var menuList = <String>[];

      snapshot.docs.forEach((doc) {
        if (doc.data()[mealType] == true) {
          menuList.add(doc.get('title').toString());
        }
      });

      menuList.shuffle();

      return menuList.take(7).toList();
    });
  }

  Future<Map<String, String>> queryWeeklyMenuStartDates() async {
    return queryWeeklyMenuSnapshot().then((snapshot) {
      var t = <String, String>{};
      snapshot.docs.forEach((doc) {
        t.addAll({
          doc.id.toString(): doc.get('start').toDate().toString().split(' ')[0]
        });
      });
      return t;
    });
  }

  Future<List<String>> queryWeeklyMenu(String docID) {
    return queryWeeklyMenuSnapshot().then((snapshot) {
      var menu = <String>[];

      snapshot.docs.forEach((doc) {
        if (doc.id == docID) {
          final fields = doc.data();
          List.generate(fields.length ~/ 3, (index) {
            final type = 'breakfast';
            final fieldName = 'day' + index.toString() + type;
            if (fields.keys.contains(fieldName)) {
              menu.add(fields[fieldName].toString());
            }
          });
        }
      });
      return menu;
    });
  }

  Future<void> addMenu(int index, String title, String reference,
      List<String> menuTypes, List<String> menuCharacter) {
    var breakfast = menuTypes.contains('Breakfast');
    var lunch = menuTypes.contains('Lunch');
    var dinner = menuTypes.contains('Dinner');

    void printErrorMessage(Object error) {
      print('Failed to add user: $error');
    }

    return users
        .doc(documentId)
        .collection('menu')
        .add(<String, Object>{
          'index': index,
          'title': title,
          'reference': reference,
          'breakfast': breakfast,
          'lunch': lunch,
          'dinner': dinner,
        })
        .then((value) => print('User Added'))
        .catchError(printErrorMessage);
  }

  Future<bool> trySignIn(String email, String password) async {
    try {
      userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      return true;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }

      return false;
    }
  }
}
